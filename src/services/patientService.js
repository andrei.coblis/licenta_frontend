import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/patients";

export function getPatients() {
  return http.get(apiEndpoint);
}
