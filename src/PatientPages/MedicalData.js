import { useState } from "react";
import { Form, FormGroup } from "react-bootstrap";
import { Button } from "@material-ui/core";
import "../styles/medicalData.css";

export default function MedicalData() {
  const [age, setAge] = useState();
  const [sex, setSex] = useState();
  const [cp, setCp] = useState();
  const [restbp, setRestbp] = useState();
  const [chol, setChol] = useState();
  const [fbs, setFbs] = useState();
  const [restecg, setRestecg] = useState();
  const [thalach, setThalach] = useState();
  const [exAngina, setExAngina] = useState();
  const [stDepression, setStDepression] = useState();
  const [slope, setSlope] = useState();
  const [nrBloodVessels, setNrBloodVessels] = useState();
  const [thalassemia, setThalassemia] = useState();

  function handleSubmit(e) {
    e.preventDefault();
    const payload = {
      age: age,
      sex: sex,
      cp: cp,
      trestbps: restbp,
      chol: chol,
      fbs: fbs,
      restecg: restecg,
      thalach: thalach,
      exang: exAngina,
      oldpeak: stDepression,
      slope: slope,
      ca: nrBloodVessels,
      thal: thalassemia,
    };

    fetch("http://127.0.0.1:5000/api", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(payload),
    }).then((response) =>
      response.json().then((responseJson) => console.log(responseJson))
    );
  }

  return (
    <div className="medicalData">
      <Form>
        <FormGroup>
          <Form.Label>Age</Form.Label>
          <Form.Control onChange={(e) => setAge(e.target.value)}></Form.Control>
          <Form.Label>Sex</Form.Label>
          <Form.Control onChange={(e) => setSex(e.target.value)}></Form.Control>
          <Form.Label>Chest Pain</Form.Label>
          <Form.Control onChange={(e) => setCp(e.target.value)}></Form.Control>
          <Form.Label>Resting Blood Pressure</Form.Label>
          <Form.Control
            onChange={(e) => setRestbp(e.target.value)}
          ></Form.Control>
          <Form.Label>Cholesterol</Form.Label>
          <Form.Control
            onChange={(e) => setChol(e.target.value)}
          ></Form.Control>
          <Form.Label>Fasting Blood Sugar</Form.Label>
          <Form.Control onChange={(e) => setFbs(e.target.value)}></Form.Control>
          <Form.Label>Resting ecg</Form.Label>
          <Form.Control
            onChange={(e) => setRestecg(e.target.value)}
          ></Form.Control>
          <Form.Label>Thalach(max heart rate achieved)</Form.Label>
          <Form.Control
            onChange={(e) => setThalach(e.target.value)}
          ></Form.Control>
          <Form.Label>Exercise angina</Form.Label>
          <Form.Control
            onChange={(e) => setExAngina(e.target.value)}
          ></Form.Control>
          <Form.Label>ST depression</Form.Label>
          <Form.Control
            onChange={(e) => setStDepression(e.target.value)}
          ></Form.Control>
          <Form.Label>Slope</Form.Label>
          <Form.Control
            onChange={(e) => setSlope(e.target.value)}
          ></Form.Control>
          <Form.Label>Nr of major blood vessels</Form.Label>
          <Form.Control
            onChange={(e) => setNrBloodVessels(e.target.value)}
          ></Form.Control>
          <Form.Label>Thalassemia</Form.Label>
          <Form.Control
            onChange={(e) => setThalassemia(e.target.value)}
          ></Form.Control>
          <Button variant="primary" type="submit" onClick={handleSubmit}>
            Submit Patient Medical Data
          </Button>
        </FormGroup>
      </Form>
    </div>
  );
}
