import { useEffect, useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import { selectUser } from "../redux/userSlice";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import "../styles/medicationPlans.css";

export default function ViewMedicationPlans() {
  const [medicationPlans, setMedicationPlans] = useState();
  const user = useSelector(selectUser);

  async function fetchData() {
    const username = user.Username;
    await axios
      .get(`https://localhost:44392/api/v1/UserMedicationPlans/${username}`)
      .then((res) => {
        console.log(res);
        setMedicationPlans(res.data);
      });
  }

  useEffect(() => {
    fetchData();
  }, []);

  const columns = [
    { dataField: "planId", text: "Id", sort: true },
    { dataField: "patientUsername", text: "Patient Username" },
    { dataField: "startDate", text: "Start Date", sort: true },
    { dataField: "endDate", text: "End Date", sort: true },
    { dataField: "lastUpdated", text: "Last Updated", sort: true },
  ];

  const { SearchBar, ClearSearchButton } = Search;

  const expandRow = {
    renderer: (row) => (
      <div>
        <p>{row.description}</p>
      </div>
    ),
    showExpandColumn: false,
  };

  return (
    <div>
      <div className="viewMedicationPlans">
        {medicationPlans && (
          <ToolkitProvider
            keyField="planId"
            bootstrap4
            data={medicationPlans}
            columns={columns}
            search
          >
            {(props) => (
              <div>
                <div className="table__header">
                  <div className="table__search">
                    <SearchBar {...props.searchProps} />
                    <ClearSearchButton {...props.searchProps} />
                  </div>
                </div>
                <hr />
                <BootstrapTable
                  {...props.baseProps}
                  pagination={paginationFactory()}
                  expandRow={expandRow}
                  striped
                  hover
                />
              </div>
            )}
          </ToolkitProvider>
        )}
      </div>
    </div>
  );
}
