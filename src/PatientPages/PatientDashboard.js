import axios from "axios";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { selectUser } from "../redux/userSlice";
import "../styles/patientDashboard.css";
import ProfilePicture from "../img/ProfilePicture.png";
import Card from "../CommonPages/Dashboard Components/Card";

export default function PatientDashboard() {
  const [medicalData, setMedicalData] = useState();
  const user = useSelector(selectUser);

  async function fetchData() {
    const username = user.Username;
    await axios
      .get(`https://localhost:44392/api/v1/UserMedicalData/${username}`)
      .then((res) => {
        setMedicalData(res.data);
      });
  }

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <main>
      <div className="main__container">
        {/* <!-- MAIN TITLE STARTS HERE --> */}

        <div className="main__title">
          <img src={ProfilePicture} alt="Profile" />
          <div>
            <div className="main__greeting">
              <h1>Hello, {user.Username}</h1>
              <p>Welcome to your dashboard</p>
            </div>
            {medicalData && (
              <div className="main__infoo">
                <p>
                  <strong>Sex: </strong>
                </p>
                <span>
                  {medicalData.sex.toString() === "true" ? "Male" : "Female"}
                </span>
                <p>
                  <strong>Status: </strong>
                </p>
                <span>
                  {medicalData.prediction.toString() === "1"
                    ? "Has heart disease"
                    : "Healthy"}
                </span>
              </div>
            )}
          </div>
        </div>

        {/* <!-- MAIN TITLE ENDS HERE --> */}

        {/* <!-- MAIN CARDS STARTS HERE --> */}
        {medicalData && (
          <div className="main__cards">
            <Card
              cardTitle="Age"
              cardValue={medicalData.age}
              tooltipText="Age in years"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Chest Pain"
              cardValue={medicalData.chestPain}
              tooltipText="0 - Typical angina: Chest pain related to decreased blood supply to the heart <br /> 
                           1 - Atypical angina: Chest pain not related to the heart <br />
                           2 - Non-anginal pain: typically esophageal spasms (not heart related) <br />
                           3 - Asymptomatic: chest pain not showing signs of disease"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Resting Blood Pressure"
              cardValue={medicalData.restBp}
              tooltipText="Measured in millimetres of mercury"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Cholesterol"
              cardValue={medicalData.cholesterol}
              tooltipText="Measured in milligrams per deciliter"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Exercise Induced Angina"
              cardValue={
                medicalData.exerciseAngina.toString() === "true" ? "Yes" : "No"
              }
              tooltipText="Angina is a type of chest pain caused by reduced blood flow to the heart"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Fasting Blood Sugar"
              cardValue={medicalData.fbs}
              tooltipText="1 - Fasting Blood Sugar higher then 120 mg/dl<br />
                           0 - Fasting Blood Sugar lower then 120 mg/dl"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Number of Blood Vessels"
              cardValue={medicalData.nrOfBloodVessels}
              tooltipText="Number of major blood vessels(0 - 3) colored by flouroscopy<br />
                           The doctor can see the blood passing through the colored vessels, the more blood movement the better"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Resting ECG"
              cardValue={medicalData.restEcg}
              tooltipText="Resting electrocardiographic results:<br />
                         0: Nothing to note<br />
                         1: ST-T Wave abnormality<br />
                         2: Possible or definite left ventricular hypertrophy"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Slope"
              cardValue={medicalData.slope}
              tooltipText="The slope of the peak exercise ST segment:<br />
                         0: Upsloping, better heart rate with excercise<br />
                         1: Flatsloping: minimal change<br />
                         2: Downsloping: signs of unhealthy heart"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="ST Depression"
              cardValue={medicalData.stDepression}
              tooltipText="ST depression induced by exercise relative to rest; looks at stress of heart during excercise"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Thalach"
              cardValue={medicalData.thalach}
              tooltipText="Maximum heart rate achieved"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Thalassemia"
              cardValue={medicalData.thalassemia}
              tooltipText="Thalium stress result:<br />
                         0, 1: normal<br />
                         2: fixed defect<br />
                         3: reversable defect:no proper blood movement when exercising"
              id={Math.random().toString()}
            />
          </div>
        )}
        {/* <!-- MAIN CARDS ENDS HERE --> */}
      </div>
    </main>
  );
}
