import { useEffect, useState } from "react";
import ActivityCard from "./Recommendation Components/ActivityCard";
import DietCard from "./Recommendation Components/DietCard";
import "../styles/recommendation.css";
import axios from "axios";
import { useSelector } from "react-redux";
import { selectUser } from "../redux/userSlice";
import MedPlanCard from "./Recommendation Components/MedPlanCard";

export default function Recommendation() {
  const [dietRecommendations, setDietRecommendations] = useState();
  const [physicalRecommendations, setPhysicalRecommendations] = useState();
  const [lastUpdatedRecommendations, setLastUpdatedRecommendations] =
    useState();
  const [medicationPlans, setMedicationPlans] = useState();
  const user = useSelector(selectUser);

  async function fetchData() {
    await axios
      .get(
        `https://localhost:44392/api/v1/UserLifestyleRecommendations/${user.Username}`
      )
      .then((response) => {
        if (response.data.length > 0) {
          setDietRecommendations(response.data[0].diet);
          setPhysicalRecommendations(response.data[0].physicalActivities);
          setLastUpdatedRecommendations(response.data[0].lastUpdated);
        }
      });

    await axios
      .get(
        `https://localhost:44392/api/v1/UserMedicationPlans/${user.Username}`
      )
      .then((response) => {
        if (response.data.length > 0) {
          setMedicationPlans(response.data);
        }
      });
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="recommendation__cards">
      {!dietRecommendations && !physicalRecommendations && !medicationPlans && (
        <h1>Nothing to show</h1>
      )}
      {dietRecommendations && (
        <DietCard
          dietRecommendations={dietRecommendations}
          lastUpdated={lastUpdatedRecommendations}
        />
      )}
      {physicalRecommendations && (
        <ActivityCard
          physicalRecommendations={physicalRecommendations}
          lastUpdated={lastUpdatedRecommendations}
        />
      )}
      {medicationPlans && <MedPlanCard medicationPlans={medicationPlans} />}
    </div>
  );
}
