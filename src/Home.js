import { Container, Col, Carousel } from "react-bootstrap";
import react from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Home1 from "./img/Home1.png";
export default function Home() {
  return (
    <div>
      <div>
        <Carousel indicators={false} controls={false}>
          <Carousel.Item>
            <img
              className="d-block w-100"
              style={{
                height: "600px",
                backgroundColor: "gray",
              }}
              src={Home1}
              alt="Home 1"
            />
          </Carousel.Item>
        </Carousel>
      </div>
    </div>
  );
}
