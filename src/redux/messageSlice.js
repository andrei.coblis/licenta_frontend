import { createSlice } from "@reduxjs/toolkit";

export const messageSlice = createSlice({
  name: "message",
  initialState: {
    selectedMessage: null,
    sendMessageIsOpen: false,
    count: 0,
  },
  reducers: {
    setCount: (state, number) => {
      state.count = number;
    },
    selectMessage: (state, action) => {
      state.selectedMessage = action.payload;
    },
    openSendMessage: (state) => {
      state.sendMessageIsOpen = true;
    },
    closeSendMessage: (state) => {
      state.sendMessageIsOpen = false;
    },
  },
});

export const { openSendMessage, closeSendMessage, selectMessage, setCount } =
  messageSlice.actions;

export const selectOpenMessage = (state) => state.message.selectedMessage;

export const selectSetCount = (state) => state.message.setCount;

export const selectSendMessageIsOpen = (state) =>
  state.message.sendMessageIsOpen;

export default messageSlice.reducer;
