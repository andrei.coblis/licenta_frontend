import { configureStore } from "@reduxjs/toolkit";
import messageReducer from "./messageSlice";
import userReducer from "./userSlice";

export default configureStore({
  reducer: {
    message: messageReducer,
    user: userReducer,
  },
});
