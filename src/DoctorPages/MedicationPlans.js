import { useEffect, useState } from "react";
import { Form, FormGroup, Table } from "react-bootstrap";
import axios from "axios";
import { Button, IconButton } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { Modal } from "react-bootstrap";
import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";
import ReactTooltip from "react-tooltip";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import "../styles/medicationPlans.css";
import Alert from "react-bootstrap/Alert";

export default function MedicationPlans() {
  const [medicationPlans, setMedicationPlans] = useState();
  const [patients, setPatients] = useState();

  const [patient, setPatient] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [description, setDescription] = useState();
  const [planId, setPlanId] = useState();

  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleClose2 = () => setShow2(false);
  const handleShow2 = () => setShow2(true);

  const [apiError, setApiError] = useState();

  async function fetchData() {
    await axios
      .get("https://localhost:44392/api/v1/medicationPlans")
      .then((res) => {
        setMedicationPlans(res.data);
      });

    await axios.get("https://localhost:44392/api/v1/patients").then((res) => {
      setPatients(res.data);
      setPatient(res.data[0].username);
    });
  }

  useEffect(() => {
    fetchData();
  }, []);

  async function handleCreateMedicationPlan(e) {
    e.preventDefault();
    var payload = {
      username: patient,
      startDate: startDate,
      endDate: endDate,
      description: description,
    };
    await axios
      .post("https://localhost:44392/api/v1/medicationPlans", payload)
      .then(() => alert('Success'))
      .catch((err) => {
        setShowAlert(true);
        handleClose();
        setApiError(
          `Failed to create medication plan. Error Status code: ${err.response.status}`
        );
      });
  }

  async function handleEditMedicationPlan(e, planId) {
    e.preventDefault();
    await axios
      .put(`https://localhost:44392/api/v1/medicationPlans/${planId}`, {
        username: patient,
        startDate: startDate,
        endDate: endDate,
        description: description,
      })
      .then(() => alert('Success'))
      .catch((err) => {setShowAlert(true);
        handleClose();
        setApiError(
          `Failed to edit medication plan. Error Status code: ${err.response.status}`
        );});
  }

  async function handleDeleteMedicationPlan(planId) {
    const medicationPlansAfterDelete = medicationPlans.filter(
      (mp) => mp.planId !== planId
    );
    await axios.delete(
      `https://localhost:44392/api/v1/medicationPlans/${planId}`
    );
    setMedicationPlans(medicationPlansAfterDelete);
  }

  function onStartDateChange(date) {
    setStartDate(date);
  }

  function onEndDateChange(date) {
    setEndDate(date);
  }

  function actionFormatter(cell, row) {
    return (
      <div>
        <IconButton data-tip data-for="edit-tooltip">
          <EditIcon
            onClick={() => {
              setPatient(row.patientUsername);
              setDescription(row.description);
              setStartDate(row.startDate);
              setEndDate(row.endDate);
              setPlanId(row.planId);
              handleShow2();
            }}
          />
        </IconButton>
        <ReactTooltip
          id="edit-tooltip"
          type="dark"
          effect="solid"
          delayShow="500"
        >
          Edit Medication Plan
        </ReactTooltip>
        <IconButton data-tip data-for="delete-tooltip">
          <DeleteIcon
            onClick={() => {
              handleDeleteMedicationPlan(row.planId);
            }}
          />
        </IconButton>
        <ReactTooltip
          id="delete-tooltip"
          type="dark"
          effect="solid"
          delayShow="500"
        >
          Delete Medication Plan
        </ReactTooltip>
      </div>
    );
  }

  const columns = [
    { dataField: "planId", text: "Id", sort: true },
    { dataField: "patientUsername", text: "Patient Username" },
    { dataField: "startDate", text: "Start Date", sort: true },
    { dataField: "endDate", text: "End Date", sort: true },
    { dataField: "lastUpdated", text: "Last Updated", sort: true },
    { dataField: "action", text: "Actions", formatter: actionFormatter },
  ];

  const { SearchBar, ClearSearchButton } = Search;

  const expandRow = {
    renderer: (row) => (
      <div>
        <p>{row.description}</p>
      </div>
    ),
    showExpandColumn: false,
  };

  function AlertAfterApiCall() {
    if (showAlert) {
      return (
        <Alert variant="danger" onClose={() => setShowAlert(false)} dismissible>
          <Alert.Heading>Something didn't quite work</Alert.Heading>
          <p>{apiError}</p>
        </Alert>
      );
    }
  }

  return (
    <div>
      {AlertAfterApiCall()}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
          }}
          closeButton
        >
          <div style={{ alignItems: "center" }}>
            <Modal.Title>Add Medication Plan</Modal.Title>
          </div>
        </Modal.Header>
        <Modal.Body>
          <Form className="text-center" style={{ width: "100%" }}>
            <FormGroup>
              <Form.Label>Select Patient</Form.Label>
              <Form.Control
                as="select"
                onChange={(e) => setPatient(e.target.value)}
              >
                {patients &&
                  patients.map((item) => {
                    return <option>{item.username}</option>;
                  })}
              </Form.Control>
              <Form.Label>Enter Description</Form.Label>
              <Form.Control
                as="textarea"
                onChange={(e) => setDescription(e.target.value)}
              ></Form.Control>
              <Form.Label>Enter Start Date</Form.Label>
              <br />
              <DayPickerInput onDayChange={onStartDateChange} />
              <br />
              <Form.Label>Enter End Date</Form.Label>
              <br />
              <DayPickerInput onDayChange={onEndDateChange} />
              <br />
              <br />
              <Button
                variant="primary"
                type="submit"
                onClick={handleCreateMedicationPlan}
              >
                Create Medication Plan
              </Button>
            </FormGroup>
          </Form>
        </Modal.Body>
      </Modal>
      <div>
        <div className="viewMedicationPlans">
          {medicationPlans && (
            <ToolkitProvider
              keyField="planId"
              bootstrap4
              data={medicationPlans}
              columns={columns}
              search
            >
              {(props) => (
                <div>
                  <div className="table__header">
                    <div className="table__search">
                      <SearchBar {...props.searchProps} />
                      <ClearSearchButton {...props.searchProps} />
                    </div>
                    <Button
                      startIcon={<AddIcon />}
                      style={{ float: "right" }}
                      onClick={() => {
                        handleShow();
                        setShowAlert(false);
                      }}
                    >
                      New Medication Plan
                    </Button>
                  </div>
                  <hr />
                  <BootstrapTable
                    {...props.baseProps}
                    pagination={paginationFactory()}
                    expandRow={expandRow}
                    striped
                    hover
                  />
                </div>
              )}
            </ToolkitProvider>
          )}
        </div>
      </div>

      <Modal show={show2} onHide={handleClose2}>
        <Modal.Header
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
          }}
          closeButton
        >
          <div style={{ alignItems: "center" }}>
            <Modal.Title>Edit Mediction Plan</Modal.Title>
          </div>
        </Modal.Header>
        <Modal.Body>
          <Form className="text-center" style={{ width: "100%" }}>
            <FormGroup>
              <Form.Label>Enter Description</Form.Label>
              <Form.Control
                as="textarea"
                defaultValue={description}
                onChange={(e) => setDescription(e.target.value)}
              ></Form.Control>
              <Form.Label>Enter Start Date</Form.Label>
              <Form.Control
                defaultValue={startDate}
                onChange={(e) => {
                  setStartDate(e.target.value);
                }}
              ></Form.Control>
              <Form.Label>Enter End Date</Form.Label>
              <Form.Control
                defaultValue={endDate}
                onChange={(e) => {
                  setEndDate(e.target.value);
                }}
              ></Form.Control>
              <Button
                variant="primary"
                type="submit"
                onClick={(e) => {
                  handleEditMedicationPlan(e, planId);
                }}
              >
                Edit Medication plan
              </Button>
            </FormGroup>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
