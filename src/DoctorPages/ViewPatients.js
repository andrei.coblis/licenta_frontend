import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import { useEffect, useState } from "react";
import axios from "axios";
import "../styles/viewPatients.css";
import { Button, IconButton } from "@material-ui/core";
import { Modal } from "react-bootstrap";
import { Form, FormGroup } from "react-bootstrap";
import EditIcon from "@material-ui/icons/Edit";
import AddIcon from "@material-ui/icons/Add";
import LocalHospitalIcon from "@material-ui/icons/LocalHospital";
import ReactTooltip from "react-tooltip";

export default function ViewPatients() {
  const [patients, setPatients] = useState();

  const algorithm = localStorage.getItem("algorithm");

  const [patientId, setPatientId] = useState();
  const [medicalDataId, setMedicalDataId] = useState();
  const [prediction, setPrediction] = useState();

  const [age, setAge] = useState();
  const [sex, setSex] = useState();
  const [cp, setCp] = useState();
  const [restbp, setRestbp] = useState();
  const [chol, setChol] = useState();
  const [fbs, setFbs] = useState();
  const [restecg, setRestecg] = useState();
  const [thalach, setThalach] = useState();
  const [exAngina, setExAngina] = useState();
  const [stDepression, setStDepression] = useState();
  const [slope, setSlope] = useState();
  const [nrBloodVessels, setNrBloodVessels] = useState();
  const [thalassemia, setThalassemia] = useState();

  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);
  const [show3, setShow3] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleClose2 = () => setShow2(false);
  const handleShow2 = () => setShow2(true);
  const handleClose3 = () => setShow3(false);
  const handleShow3 = () => setShow3(true);

  async function fetchData() {
    await axios.get("https://localhost:44392/api/v1/patients").then((res) => {
      setPatients(res.data);
    });
  }

  async function getPatientMedicalData(username) {
    await axios
      .get(`https://localhost:44392/api/v1/UserMedicalData/${username}`)
      .then((res) => {
        setAge(res.data.age);
        setSex(res.data.sex);
        setCp(res.data.chestPain);
        setRestbp(res.data.restBp);
        setChol(res.data.cholesterol);
        setFbs(res.data.fbs);
        setRestecg(res.data.restEcg);
        setThalach(res.data.thalach);
        setExAngina(res.data.exerciseAngina);
        setStDepression(res.data.stDepression);
        setSlope(res.data.slope);
        setNrBloodVessels(res.data.nrOfBloodVessels);
        setThalassemia(res.data.thalassemia);
      });
  }

  async function runPrediction(username) {
    var payload = {};
    await axios
      .get(`https://localhost:44392/api/v1/UserMedicalData/${username}`)
      .then((res) => {
        payload = {
          age: res.data.age,
          sex: res.data.sex,
          cp: res.data.chestPain,
          trestbps: res.data.restBp,
          chol: res.data.cholesterol,
          fbs: res.data.fbs,
          restecg: res.data.restEcg,
          thalach: res.data.thalach,
          exang: res.data.exerciseAngina,
          oldpeak: res.data.stDepression,
          slope: res.data.slope,
          ca: res.data.nrOfBloodVessels,
          thal: res.data.thalassemia,
          algorithm: algorithm,
        };
      });

    fetch(`http://127.0.0.1:5000/makePrediction`, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(payload),
    }).then((res) =>
      res.json().then((responseJson) => setPrediction(responseJson["result"]))
    );
  }

  async function handleCreateMedicalData(e) {
    e.preventDefault();
    await axios.post("https://localhost:44392/api/v1/medicalData", {
      age: age,
      sex: sex,
      chestPain: cp,
      restBp: restbp,
      cholesterol: chol,
      fbs: fbs,
      restEcg: restecg,
      thalach: thalach,
      exerciseAngina: exAngina,
      stDepression: stDepression,
      slope: slope,
      nrOfBloodVessels: nrBloodVessels,
      thalassemia: thalassemia,
      patientId: patientId,
    });
  }

  async function handleEditMedicalData(e) {
    e.preventDefault();
    await axios.put(
      `https://localhost:44392/api/v1/medicalData/${medicalDataId}`,
      {
        age: age,
        sex: sex,
        chestPain: cp,
        restBp: restbp,
        cholesterol: chol,
        fbs: fbs,
        restEcg: restecg,
        thalach: thalach,
        exerciseAngina: exAngina,
        stDepression: stDepression,
        slope: slope,
        nrOfBloodVessels: nrBloodVessels,
        thalassemia: thalassemia,
        patientId: patientId,
      }
    );
  }

  useEffect(() => {
    fetchData();
  }, []);

  function hasMedicalDataFormatter(cell, row) {
    if (cell.toString() === "false") {
      return (
        <span>
          <strong style={{ color: "red" }}>NO</strong>
        </span>
      );
    }
    return (
      <span>
        <strong style={{ color: "green" }}>Yes</strong>
      </span>
    );
  }

  function buttonFormatter(cell, row) {
    if (row.hasMedicalData === false) {
      return (
        <div>
          <IconButton data-tip data-for="add-tooltip">
            <AddIcon
              onClick={() => {
                setPatientId(row.id);
                handleShow();
              }}
            />
          </IconButton>
          <ReactTooltip
            id="add-tooltip"
            type="dark"
            effect="solid"
            delayShow="500"
          >
            Submit Medical Data
          </ReactTooltip>
        </div>
      );
    } else {
      return (
        <div>
          <IconButton data-tip data-for="edit-tooltip">
            <EditIcon
              onClick={() => {
                getPatientMedicalData(row.username);
                setMedicalDataId(row.medicalDataId);
                setPatientId(row.id);
                handleShow2();
              }}
            />
          </IconButton>
          <ReactTooltip
            id="edit-tooltip"
            type="dark"
            effect="solid"
            delayShow="500"
          >
            Edit Medical Data
          </ReactTooltip>

          <IconButton>
            <LocalHospitalIcon
              data-tip
              data-for="medicalStatus-tooltip"
              onClick={() => {
                getPatientMedicalData(row.username);
                runPrediction(row.username);
                handleShow3();
              }}
            />
          </IconButton>
          <ReactTooltip
            id="medicalStatus-tooltip"
            type="dark"
            effect="solid"
            delayShow="500"
          >
            View Medical Status
          </ReactTooltip>
        </div>
      );
    }
  }

  const columns = [
    {
      dataField: "id",
      text: "Patient Id",
      sort: true,
    },
    {
      dataField: "username",
      text: "Username",
    },
    {
      dataField: "firstName",
      text: "First Name",
    },
    {
      dataField: "lastName",
      text: "Last Name",
    },
    {
      dataField: "gender",
      text: "Gender",
      sort: true,
    },
    {
      dataField: "birthDate",
      text: "Birth Date",
      sort: true,
    },
    {
      dataField: "hasMedicalData",
      text: "Medical Data",
      sort: true,
      formatter: hasMedicalDataFormatter,
    },
    {
      dataField: "action",
      text: "Action",
      formatter: buttonFormatter,
    },
    {
      dataField: "medicalDataId",
      text: "Medical Data Id",
      hidden: "true",
    },
  ];

  const defaultSorted = [
    {
      dataField: "hasMedicalData",
      order: "asc",
    },
  ];

  const { SearchBar, ClearSearchButton } = Search;

  return (
    <div>
      <div className="viewPatients">
        {patients && (
          <ToolkitProvider
            keyField="id"
            bootstrap4
            data={patients}
            columns={columns}
            search
          >
            {(props) => (
              <div>
                <SearchBar {...props.searchProps} />
                <ClearSearchButton {...props.searchProps} />
                <hr />
                <BootstrapTable
                  {...props.baseProps}
                  pagination={paginationFactory()}
                  striped
                  hover
                  defaultSorted={defaultSorted}
                />
              </div>
            )}
          </ToolkitProvider>
        )}
      </div>

      {/* Add Medical Data Form and Modal Begins */}

      <div>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              textAlign: "center",
            }}
            closeButton
          >
            <div style={{ alignItems: "center" }}>
              <Modal.Title>Add Medical Data</Modal.Title>
            </div>
          </Modal.Header>
          <Modal.Body>
            <Form className="text-center" style={{ width: "100%" }}>
              <FormGroup>
                <Form.Label>Age</Form.Label>
                <Form.Control
                  onChange={(e) => setAge(e.target.value)}
                ></Form.Control>
                <Form.Label>Sex</Form.Label>
                <Form.Control
                  onChange={(e) => setSex(e.target.value)}
                ></Form.Control>
                <Form.Label>Chest Pain</Form.Label>
                <Form.Control
                  onChange={(e) => setCp(e.target.value)}
                ></Form.Control>
                <Form.Label>Resting Blood Pressure</Form.Label>
                <Form.Control
                  onChange={(e) => setRestbp(e.target.value)}
                ></Form.Control>
                <Form.Label>Cholesterol</Form.Label>
                <Form.Control
                  onChange={(e) => setChol(e.target.value)}
                ></Form.Control>
                <Form.Label>Fasting Blood Sugar</Form.Label>
                <Form.Control
                  onChange={(e) => setFbs(e.target.value)}
                ></Form.Control>
                <Form.Label>Resting ecg</Form.Label>
                <Form.Control
                  onChange={(e) => setRestecg(e.target.value)}
                ></Form.Control>
                <Form.Label>Thalach(max heart rate achieved)</Form.Label>
                <Form.Control
                  onChange={(e) => setThalach(e.target.value)}
                ></Form.Control>
                <Form.Label>Exercise angina</Form.Label>
                <Form.Control
                  onChange={(e) => setExAngina(e.target.value)}
                ></Form.Control>
                <Form.Label>ST depression</Form.Label>
                <Form.Control
                  onChange={(e) => setStDepression(e.target.value)}
                ></Form.Control>
                <Form.Label>Slope</Form.Label>
                <Form.Control
                  onChange={(e) => setSlope(e.target.value)}
                ></Form.Control>
                <Form.Label>Nr of major blood vessels</Form.Label>
                <Form.Control
                  onChange={(e) => setNrBloodVessels(e.target.value)}
                ></Form.Control>
                <Form.Label>Thalassemia</Form.Label>
                <Form.Control
                  onChange={(e) => setThalassemia(e.target.value)}
                ></Form.Control>
                <Button
                  variant="primary"
                  type="submit"
                  onClick={handleCreateMedicalData}
                >
                  Submit Patient Medical Data
                </Button>
              </FormGroup>
            </Form>
          </Modal.Body>
        </Modal>

        <Modal show={show3} onHide={handleClose3}>
          <Modal.Header
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              textAlign: "center",
            }}
            closeButton
          >
            <div style={{ alignItems: "center" }}>
              <Modal.Title>View Prediction</Modal.Title>
            </div>
          </Modal.Header>
          <Modal.Body>
            <Form className="text-center" style={{ width: "100%" }}>
              <FormGroup>
                <Form.Label>{prediction}</Form.Label>
                <br />
                <Button
                  onClick={() => (window.location = "/MakeRecommendations")}
                >
                  Go to Recommendations
                </Button>
                <Button onClick={() => (window.location = "/MedicationPlans")}>
                  Go to Medication Plans
                </Button>
              </FormGroup>
            </Form>
          </Modal.Body>
        </Modal>

        {/* Edit medical data form and modal begins */}

        <Modal show={show2} onHide={handleClose2}>
          <Modal.Header
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              textAlign: "center",
            }}
            closeButton
          >
            <div style={{ alignItems: "center" }}>
              <Modal.Title>Edit Medical Data</Modal.Title>
            </div>
          </Modal.Header>
          <Modal.Body>
            <Form className="text-center" style={{ width: "100%" }}>
              <FormGroup>
                <Form.Label>Age</Form.Label>
                <Form.Control
                  defaultValue={age}
                  onChange={(e) => setAge(e.target.value)}
                ></Form.Control>
                <Form.Label>Sex</Form.Label>
                <Form.Control
                  defaultValue={sex}
                  onChange={(e) => setSex(e.target.value)}
                ></Form.Control>
                <Form.Label>Chest Pain</Form.Label>
                <Form.Control
                  defaultValue={cp}
                  onChange={(e) => setCp(e.target.value)}
                ></Form.Control>
                <Form.Label>Resting Blood Pressure</Form.Label>
                <Form.Control
                  defaultValue={restbp}
                  onChange={(e) => setRestbp(e.target.value)}
                ></Form.Control>
                <Form.Label>Cholesterol</Form.Label>
                <Form.Control
                  defaultValue={chol}
                  onChange={(e) => setChol(e.target.value)}
                ></Form.Control>
                <Form.Label>Fasting Blood Sugar</Form.Label>
                <Form.Control
                  defaultValue={fbs}
                  onChange={(e) => setFbs(e.target.value)}
                ></Form.Control>
                <Form.Label>Resting ecg</Form.Label>
                <Form.Control
                  defaultValue={restecg}
                  onChange={(e) => setRestecg(e.target.value)}
                ></Form.Control>
                <Form.Label>Thalach(max heart rate achieved)</Form.Label>
                <Form.Control
                  defaultValue={thalach}
                  onChange={(e) => setThalach(e.target.value)}
                ></Form.Control>
                <Form.Label>Exercise angina</Form.Label>
                <Form.Control
                  defaultValue={exAngina}
                  onChange={(e) => setExAngina(e.target.value)}
                ></Form.Control>
                <Form.Label>ST depression</Form.Label>
                <Form.Control
                  defaultValue={stDepression}
                  onChange={(e) => setStDepression(e.target.value)}
                ></Form.Control>
                <Form.Label>Slope</Form.Label>
                <Form.Control
                  defaultValue={slope}
                  onChange={(e) => setSlope(e.target.value)}
                ></Form.Control>
                <Form.Label>Nr of major blood vessels</Form.Label>
                <Form.Control
                  defaultValue={nrBloodVessels}
                  onChange={(e) => setNrBloodVessels(e.target.value)}
                ></Form.Control>
                <Form.Label>Thalassemia</Form.Label>
                <Form.Control
                  defaultValue={thalassemia}
                  onChange={(e) => setThalassemia(e.target.value)}
                ></Form.Control>
                <Button
                  variant="primary"
                  type="submit"
                  onClick={handleEditMedicalData}
                >
                  Edit Patient Medical Data
                </Button>
              </FormGroup>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    </div>
  );
}
