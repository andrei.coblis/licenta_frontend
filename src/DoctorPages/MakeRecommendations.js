import axios from "axios";
import { useEffect, useState } from "react";
import { Form, FormGroup, Modal } from "react-bootstrap";
import { Button, IconButton } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import "../styles/makeRecommendations.css";
import ReactTooltip from "react-tooltip";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

export default function MakeRecommendations() {
  const [patients, setPatients] = useState();
  const [recommendations, setRecommendations] = useState();

  const [patient, setPatient] = useState("NewPatient");
  const [patientId, setPatientId] = useState();
  const [diet, setDiet] = useState();
  const [physicalActivity, setPhysicalActivity] = useState();
  const [recommendationId, setRecommendationId] = useState();

  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleClose2 = () => setShow2(false);
  const handleShow2 = () => setShow2(true);

  async function fetchData() {
    await axios.get("https://localhost:44392/api/v1/patients").then((res) => {
      setPatients(res.data);
      console.log(res.data);
    });

    await axios
      .get("https://localhost:44392/api/v1/lifestyleRecommendations")
      .then((res) => {
        setRecommendations(res.data);
      });
  }

  useEffect(() => {
    fetchData();
  }, []);

  async function handleCreateRecommendation(e) {
    e.preventDefault();
    await axios.post(
      "https://localhost:44392/api/v1/lifestyleRecommendations",
      {
        diet: diet,
        physicalActivities: physicalActivity,
        patientId: patientId,
      }
    ).then(() => alert('Success'))
    .catch((err) => alert(err));;
  }

  async function handleEditRecommendations(e, recommendationId) {
    e.preventDefault();
    await axios.put(
      `https://localhost:44392/api/v1/lifestyleRecommendations/${recommendationId}`,
      {
        patientId: patientId,
        diet: diet,
        physicalActivities: physicalActivity,
      }
    ).then(() => alert('Success'))
    .catch((err) => alert(err));;
  }

  async function handleDeleteRecommendations(recommendationId) {
    const recommendationsAfterDelete = recommendations.filter(
      (r) => r.lifestyleRecommendationId !== recommendationId
    );
    await axios.delete(
      `https://localhost:44392/api/v1/lifestyleRecommendations/${recommendationId}`
    );
    setRecommendations(recommendationsAfterDelete);
  }

  function actionFormatter(cell, row) {
    return (
      <div>
        <IconButton data-tip data-for="edit-tooltip">
          <EditIcon
            onClick={() => {
              handleShow2();
              setPatientId(row.patientId);
              setDiet(row.diet);
              setPhysicalActivity(row.physicalActivities);
              setRecommendationId(row.lifestyleRecommendationId);
            }}
          />
        </IconButton>
        <ReactTooltip
          id="edit-tooltip"
          type="dark"
          effect="solid"
          delayShow="500"
        >
          Edit Recommendations
        </ReactTooltip>

        <IconButton data-tip data-for="delete-tooltip">
          <DeleteIcon
            onClick={() => {
              handleDeleteRecommendations(row.lifestyleRecommendationId);
            }}
          />
        </IconButton>
        <ReactTooltip
          id="delete-tooltip"
          type="dark"
          effect="solid"
          delayShow="500"
        >
          Delete Recommendations
        </ReactTooltip>
      </div>
    );
  }

  const columns = [
    { dataField: "lifestyleRecommendationId", text: "Id", sort: true },
    { dataField: "patientUsername", text: "Patient Username" },
    { dataField: "lastUpdated", text: "Last Updated", sort: true },
    { dataField: "action", text: "Actions", formatter: actionFormatter },
  ];

  const { SearchBar, ClearSearchButton } = Search;

  const expandRow = {
    renderer: (row) => (
      <div>
        <p>
          <strong>Diet Recommendations: </strong>
          {row.diet}
        </p>
        <p>
          <strong>Physical Activity Recommendations: </strong>
          {row.physicalActivities}
        </p>
      </div>
    ),
    showExpandColumn: false,
  };

  return (
    <div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
          }}
          closeButton
        >
          <div style={{ alignItems: "center" }}>
            <Modal.Title>Add Recommendations</Modal.Title>
          </div>
        </Modal.Header>
        <Modal.Body>
          <Form className="text-center" style={{ width: "100%" }}>
            <FormGroup>
              <Form.Label>Select Patient</Form.Label>
              <Form.Control
                as="select"
                onChange={(e) => setPatientId(e.target.value)}
              >
                {patients &&
                  patients.map((item) => {
                    return <option>{item.id}</option>;
                  })}
              </Form.Control>
              <Form.Label>Diet Recommendation</Form.Label>
              <Form.Control
                as="textarea"
                onChange={(e) => setDiet(e.target.value)}
              ></Form.Control>
              <Form.Label>Physical Activity Recommendation</Form.Label>
              <Form.Control
                as="textarea"
                onChange={(e) => setPhysicalActivity(e.target.value)}
              ></Form.Control>
              <Button
                variant="primary"
                type="submit"
                onClick={handleCreateRecommendation}
              >
                Make Recommendation
              </Button>
            </FormGroup>
          </Form>
        </Modal.Body>
      </Modal>
      <div>
        <div className="makeRecommendations">
          {recommendations && (
            <ToolkitProvider
              keyField="id"
              bootstrap4
              data={recommendations}
              columns={columns}
              search
            >
              {(props) => (
                <div>
                  <div className="table__header">
                    <div className="table__search">
                      <SearchBar {...props.searchProps} />
                      <ClearSearchButton {...props.searchProps} />
                    </div>
                    <Button startIcon={<AddIcon />} onClick={handleShow}>
                      New Recommendation
                    </Button>
                  </div>
                  <hr />
                  <BootstrapTable
                    {...props.baseProps}
                    pagination={paginationFactory()}
                    expandRow={expandRow}
                    striped
                    hover
                  />
                </div>
              )}
            </ToolkitProvider>
          )}
        </div>
      </div>

      <Modal show={show2} onHide={handleClose2}>
        <Modal.Header
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
          }}
          closeButton
        >
          <div style={{ alignItems: "center" }}>
            <Modal.Title>Edit Recommendations</Modal.Title>
          </div>
        </Modal.Header>
        <Modal.Body>
          <Form className="text-center" style={{ width: "100%" }}>
            <FormGroup>
              <Form.Label>Enter Diet</Form.Label>
              <Form.Control
                as="textarea"
                onChange={(e) => setDiet(e.target.value)}
                defaultValue={diet}
              ></Form.Control>
              <Form.Label>Enter Physical Activity</Form.Label>
              <Form.Control
                as="textarea"
                onChange={(e) => setPhysicalActivity(e.target.value)}
                defaultValue={physicalActivity}
              ></Form.Control>
              <Button
                variant="primary"
                type="submit"
                onClick={(e) => {
                  handleEditRecommendations(e, recommendationId);
                }}
              >
                Edit Recommendations
              </Button>
            </FormGroup>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
