import { useSelector } from "react-redux";
import { selectUser } from "../redux/userSlice";
import ProfilePicture from "../img/ProfilePicture.png";
import Card from "../CommonPages/Dashboard Components/Card";
import PieChart from "../CommonPages/Dashboard Components/PieChart";
import BarChart from "../CommonPages/Dashboard Components/BarChart";
import "../styles/doctorDashboard.css";
import { useEffect, useState } from "react";
import Select from "react-select";
import axios from "axios";

export default function DoctorDashboard() {
  const user = useSelector(selectUser);
  const algorithm = localStorage.getItem("algorithm");
  const [pieChart, setPieChart] = useState("Sex");
  const [barChart, setBarChart] = useState("Age");
  const [medicalDataStats, setMedicalDataStats] = useState();
  const [doctorStatistics, setDoctorStatistics] = useState();

  const pieChartOptions = [
    { value: "Sex", label: "Sex" },
    { value: "Chest Pain", label: "Chest Pain" },
    { value: "Exercise Induced Angina", label: "Exercise Induced Angina" },
    { value: "Number of Blood Vessels", label: "Number of Blood Vessels" },
    { value: "Resting ECG", label: "Resting ECG" },
    { value: "Slope", label: "Slope" },
    { value: "Thalassemia", label: "Thalassemia" },
  ];

  const barChartOptions = [
    { value: "Age", label: "Age" },
    { value: "Resting Blood Pressure", label: "Resting Blood Pressure" },
    { value: "Cholesterol", label: "Cholesterol" },
    { value: "Maximum Heart Rate", label: "Maximum Heart Rate" },
  ];

  function renderPieChart() {
    if (pieChart === "Sex")
      return (
        <PieChart
          labels={["Male", "Female"]}
          data={[medicalDataStats.maleCount, medicalDataStats.femaleCount]}
        />
      );

    if (pieChart === "Chest Pain")
      return (
        <PieChart
          labels={[
            "Typical Angina",
            "Atypical Angina",
            "Non-Anginal Pain",
            "Asymptomatic",
          ]}
          data={[
            medicalDataStats.chestPain0Count,
            medicalDataStats.chestPain1Count,
            medicalDataStats.chestPain2Count,
            medicalDataStats.chestPain3Count,
          ]}
        />
      );
    if (pieChart === "Exercise Induced Angina")
      return (
        <PieChart
          labels={[
            "Has exercise induced angina",
            "Doesn't have exercise induced angina",
          ]}
          data={[
            medicalDataStats.exerciseAnginaTrueCount,
            medicalDataStats.exerciseAnginaFalseCount,
          ]}
        />
      );
    if (pieChart === "Number of Blood Vessels")
      return (
        <PieChart
          labels={[
            "Has 0 blood vessels colored",
            "Has 1 blood vessels colored",
            "Has 2 blood vessels colored",
            "Has 3 blood vessels colored",
          ]}
          data={[
            medicalDataStats.nrOfBloodVessels0Count,
            medicalDataStats.nrOfBloodVessels1Count,
            medicalDataStats.nrOfBloodVessels2Count,
            medicalDataStats.nrOfBloodVessels3Count,
          ]}
        />
      );
    if (pieChart === "Resting ECG")
      return (
        <PieChart
          labels={[
            "Nothing to note",
            "ST-T wave abnormality",
            "Possible or definite left ventricular hypertrophy",
          ]}
          data={[
            medicalDataStats.restEcg0Count,
            medicalDataStats.restEcg1Count,
            medicalDataStats.restEcg2Count,
          ]}
        />
      );
    if (pieChart === "Slope")
      return (
        <PieChart
          labels={["Upsloping", "Flatsloping", "Downsloping"]}
          data={[
            medicalDataStats.slope0Count,
            medicalDataStats.slope1Count,
            medicalDataStats.slope2Count,
          ]}
        />
      );
    if (pieChart === "Thalassemia")
      return (
        <PieChart
          labels={["Normal", "Fixed Defect", "Reversable Defect"]}
          data={[
            medicalDataStats.thalassemia0Count +
              medicalDataStats.thalassemia1Count,
            medicalDataStats.thalassemia2Count,
            medicalDataStats.thalassemia3Count,
          ]}
        />
      );
  }

  function renderBarChart() {
    if (barChart === "Age")
      return (
        <BarChart
          labels={Array.from(Array(100).keys())}
          data={medicalDataStats.ageHistogram}
          label="Age Histogram"
        />
      );
    if (barChart === "Resting Blood Pressure")
      return (
        <BarChart
          labels={Array.from(Array(220).keys())}
          data={medicalDataStats.restbpHistogram}
          label="Resting Blood Pressure Histogram"
        />
      );
    if (barChart === "Cholesterol")
      return (
        <BarChart
          labels={Array.from(Array(600).keys())}
          data={medicalDataStats.cholesterolHistogram}
          label="Cholesterol Histogram"
        />
      );
    if (barChart === "Maximum Heart Rate")
      return (
        <BarChart
          labels={Array.from(Array(220).keys())}
          data={medicalDataStats.thalachHistogram}
          label="Maximum Heart Rate Histogram"
        />
      );
  }
  async function fetchData() {
    await axios
      .get("https://localhost:44392/api/v1/medicalData/statistics")
      .then((res) => {
        setMedicalDataStats(res.data);
      });
    await axios
      .get("https://localhost:44392/api/v1/doctors/statistics")
      .then((res) => {
        setDoctorStatistics(res.data);
      });
  }
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <main>
      <div className="main__container">
        {/* <!-- MAIN TITLE STARTS HERE --> */}

        <div className="main__title">
          <img src={ProfilePicture} alt="Profile" />
          <div>
            <div className="main__greeting">
              <h1>Hello, {user.Username}</h1>
              <p>Welcome to your dashboard</p>
            </div>
          </div>
        </div>

        {/* <!-- MAIN TITLE ENDS HERE --> */}

        {/* <!-- MAIN CARDS STARTS HERE --> */}
        {doctorStatistics && (
          <div className="main__cards">
            <Card
              cardTitle="Nr of Patients"
              cardValue={doctorStatistics.nrOfPatients}
              tooltipText="Nr of patients"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Nr of Patients with Medical Data"
              cardValue={doctorStatistics.nrOfPatientsWithMedicalData}
              tooltipText="Nr of patients with Medical Data"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Nr of Patients without Medical Data"
              cardValue={doctorStatistics.nrOfPatientsWithoutMedicalData}
              tooltipText="Nr of patients without Medical Data"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Nr of predicted sick patients"
              cardValue={doctorStatistics.nrOfPredictedSickPatients}
              tooltipText="Nr of predicted sick patients"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Nr of predicted healthy patients"
              cardValue={doctorStatistics.nrOfPredictedHealthyPaients}
              tooltipText="Nr of predicted healthy patients"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Nr of recommendations made"
              cardValue={doctorStatistics.nrOfRecommendations}
              tooltipText="Nr of recommendations made"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Nr of med plans prescribed"
              cardValue={doctorStatistics.nrOfMedPlans}
              tooltipText="Nr of med plans prescribed"
              id={Math.random().toString()}
            />
            <Card
              cardTitle="Algorithm used for prediction"
              cardValue="Logistic Regression"
              tooltipText="Algorithm used for prediction"
              id={Math.random().toString()}
            />
          </div>
        )}

        {/* <!-- MAIN CARDS ENDS HERE --> */}

        {/* <!-- CHARTS STARTS HERE --> */}
        {medicalDataStats && (
          <div className="charts">
            <div className="charts__left">
              <div className="charts__left__title">
                <div>
                  <h1>Patient Statistics</h1>
                  <p>Sex, Chest Pain, Exercise Induced Angina, etc</p>
                  <Select
                    defaultValue={{ value: "Sex", label: "Sex" }}
                    options={pieChartOptions}
                    onChange={(e) => {
                      setPieChart(e.value);
                    }}
                  />
                </div>
              </div>
              {renderPieChart()}
            </div>
            <div className="charts__right">
              <div className="charts__right__title">
                <div>
                  <h1>Patient Histograms</h1>
                  <p>Age, Cholesterol, Fasting Blood Sugar, etc</p>
                  <Select
                    defaultValue={{ value: "Age", label: "Age" }}
                    options={barChartOptions}
                    onChange={(e) => {
                      setBarChart(e.value);
                    }}
                  />
                </div>
              </div>
              {renderBarChart()}
            </div>
          </div>
        )}
      </div>
    </main>
  );
}
