import React, { useEffect, useState } from "react";
import "./styles/App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import Navbar from "./CommonPages/Navbar";
import SendMessage from "./CommonPages/Inbox Components/SendMessage";
import { useSelector } from "react-redux";
import { selectSendMessageIsOpen } from "./redux/messageSlice";
import { selectUser } from "./redux/userSlice";
import { IconButton } from "@material-ui/core";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import TwitterIcon from "@material-ui/icons/Twitter";
import RedditIcon from "@material-ui/icons/Reddit";
import authService from "./services/authService";

function App() {
  const sendMessageIsOpen = useSelector(selectSendMessageIsOpen);
  const user = useSelector(selectUser);
  const [userInfo, setUserInfo] = useState();

  useEffect(() => {
    const user = authService.getCurrentUser();
    setUserInfo(user);
  }, []);

  return (
    <div className="App">
      <header>
        <Navbar user={userInfo} />
        {sendMessageIsOpen && <SendMessage />}
      </header>
      <div className="footer">
        <div className="footer__icons">
          <IconButton>
            <FacebookIcon
              onClick={() => {
                console.log("facebook");
              }}
            />
          </IconButton>
          <IconButton>
            <InstagramIcon
              onClick={() => {
                console.log("instagram");
              }}
            />
          </IconButton>
          <IconButton>
            <TwitterIcon
              onClick={() => {
                console.log("twitter");
              }}
            />
          </IconButton>
          <IconButton>
            <RedditIcon
              onClick={() => {
                console.log("reddit");
              }}
            />
          </IconButton>
        </div>
        <div className="footer__text">
          Adresa Clinica: Str Romulus Vuia nr 133
          <br />
          Cluj-Napoca, Cluj, Romania
        </div>
      </div>
    </div>
  );
}

export default App;
