import React from "react";
import { Route, Switch, Link, BrowserRouter as Router } from "react-router-dom";
import Home from "../Home";
import Login from "./Login";
import Inbox from "./Inbox";
import Message from "./Inbox Components/Message";
import Logout from "./Logout";
import { Modal } from "react-bootstrap";
import { useState } from "react";
import Patients from "../AdminPages/Patients";
import MedicationPlans from "../DoctorPages/MedicationPlans";
import PatientDashboard from "../PatientPages/PatientDashboard";
import Recommendation from "../PatientPages/Recommendation";
import MakeRecommendations from "../DoctorPages/MakeRecommendations";
import ViewMedicationPlans from "../PatientPages/ViewMedicationPlans";
import ViewPatients from "../DoctorPages/ViewPatients";
import MedicalData from "../PatientPages/MedicalData";
import Doctors from "../AdminPages/Doctors";
import Admins from "../AdminPages/Admins";
import DoctorDashboard from "../DoctorPages/DoctorDashboard";
import AdminDashboard from "../AdminPages/AdminDashboard";
const Navbar = ({ user }) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <Router>
      {console.log(user)}
      <div className="nav-bar">
        {!user && (
          <ul>
            <li className="nav-bar-item" id="nav-bar-home">
              <Link to="/">Home</Link>
            </li>
            <li className="nav-bar-item" style={{ cursor: "pointer" }}>
              <a onClick={handleShow}>Login</a>

              <Modal show={show} onHide={handleClose}>
                <Modal.Header>
                  <Login />
                </Modal.Header>
              </Modal>
            </li>
          </ul>
        )}
        {user && user.Role.toLowerCase() === "patient" && (
          <ul>
            <li className="nav-bar-item" id="nav-bar-home">
              <Link to="/">Home</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/Logout">Logout</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/PatientDashboard">Dashboard</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/Recommendation">Recommendations</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/ViewMedicationPlans">Medical Plans</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/Inbox">Inbox</Link>
            </li>
          </ul>
        )}
        {user && user.Role.toLowerCase() === "doctor" && (
          <ul>
            <li className="nav-bar-item" id="nav-bar-home">
              <Link to="/">Home</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/Logout">Logout</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/ViewPatients">Patients</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/DoctorDashboard">Dashboard</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/MedicationPlans">Medical Plans</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/MakeRecommendations">Recommendations</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/Inbox">Inbox</Link>
            </li>
          </ul>
        )}
        {user && user.Role.toLowerCase() === "admin" && (
          <ul>
            <li className="nav-bar-item" id="nav-bar-home">
              <Link to="/">Home</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/Logout">Logout</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/AdminDashboard">Dashboard</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/Patients">Pacients</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/Doctors">Doctors</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/Admins">Admins</Link>
            </li>
            <li className="nav-bar-item">
              <Link to="/Inbox">Inbox</Link>
            </li>
          </ul>
        )}
      </div>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/Inbox">
          <Inbox />
        </Route>
        <Route exact path="/Message">
          <Message />
        </Route>
        <Route exact path="/Logout">
          <Logout />
        </Route>
        <Route exact path="/Patients">
          <Patients />
        </Route>
        <Route exact path="/MedicationPlans">
          <MedicationPlans />
        </Route>
        <Route exact path="/MedicalData">
          <MedicalData />
        </Route>
        <Route exact path="/PatientDashboard">
          <PatientDashboard />
        </Route>
        <Route exact path="/Recommendation">
          <Recommendation />
        </Route>
        <Route exact path="/MakeRecommendations">
          <MakeRecommendations />
        </Route>
        <Route exact path="/ViewMedicationPlans">
          <ViewMedicationPlans />
        </Route>
        <Route exact path="/ViewPatients">
          <ViewPatients />
        </Route>
        <Route exact path="/Doctors">
          <Doctors />
        </Route>
        <Route exact path="/Admins">
          <Admins />
        </Route>
        <Route exact path="/DoctorDashboard">
          <DoctorDashboard />
        </Route>
        <Route exact path="/AdminDashboard">
          <AdminDashboard />
        </Route>
      </Switch>
    </Router>
  );
};

export default Navbar;
