import React from "react";
import "../../styles/messageList.css";
import MessageRow from "./MessageRow";

export default function MessageList({ messages }) {
  return (
    <div className="messageList">
      {messages &&
        messages.map((item) => {
          return (
            <MessageRow
              id={item.messageId}
              title={item.sender}
              subject={item.subject}
              description={item.description}
              time={item.sentDate}
            />
          );
        })}
    </div>
  );
}
