import { Checkbox } from "@material-ui/core";
import React from "react";
import "../../styles/messageRow.css";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { selectMessage } from "../../redux/messageSlice";

export default function MessageRow({ id, title, subject, description, time }) {
  const history = useHistory();
  const dispatch = useDispatch();

  const openMessage = () => {
    dispatch(
      selectMessage({
        id,
        title,
        subject,
        description,
        time,
      })
    );

    history.push("/Message");
  };

  return (
    <div onClick={openMessage} className="messageRow">
      <div className="messageRow__options">
        <Checkbox />
      </div>
      <h3 className="messageRow__title">{title}</h3>
      <div className="messageRow__message">
        <h4>
          {subject}
          <span className="messageRow__description"> - {description}</span>
        </h4>
      </div>
      <p className="messageRow__time">{time}</p>
    </div>
  );
}
