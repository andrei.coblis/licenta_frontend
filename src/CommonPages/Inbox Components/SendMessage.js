import React from "react";
import "../../styles/sendMessage.css";
import CloseIcon from "@material-ui/icons/Close";
import { Button } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { closeSendMessage } from "../../redux/messageSlice";
import axios from "axios";
import { selectUser } from "../../redux/userSlice";

function SendMessage() {
  const { register, handleSubmit, watch, errors } = useForm();
  const user = useSelector(selectUser);
  const dispatch = useDispatch();

  async function handleSendMessage(data) {
    console.log(data);
    await axios.post("https://localhost:44392/api/v1/messages", {
      username: data.to,
      sender: user.Username,
      subject: data.subject,
      description: data.message,
      sentDate: new Date(),
    }).then((response) => alert(response.data))
    .catch((err) => alert(err));
  }

  const onSubmit = (formData) => {
    handleSendMessage(formData);
  };

  return (
    <div className="sendMessage">
      <div className="sendMessage__header">
        <h3>New Message</h3>
        <CloseIcon
          className="sendMessage__close"
          onClick={() => dispatch(closeSendMessage())}
        />
      </div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <input
          name="to"
          placeholder="To"
          type="text"
          ref={register({ required: true })}
        />
        {errors.to && <p className="sendMessage__error">To is required!</p>}
        <input
          name="subject"
          placeholder="Subject"
          type="text"
          ref={register({ required: true })}
        />
        {errors.subject && (
          <p className="sendMessage__error">Subject is required!</p>
        )}
        <input
          name="message"
          placeholder="Message..."
          className="sendMessage__message"
          type="text"
          ref={register({ required: true })}
        />
        {errors.message && (
          <p className="sendMessage__error">Message is required!</p>
        )}
        <div className="sendMessage__options">
          <Button
            className="sendMessage__send"
            variant="contained"
            color="primary"
            type="submit"
          >
            Send
          </Button>
        </div>
      </form>
    </div>
  );
}

export default SendMessage;
