import { Button } from "@material-ui/core";
import React from "react";
import "../../styles/sidebar.css";
import InboxIcon from "@material-ui/icons/Inbox";
import SidebarOption from "./SidebarOption";
import AddIcon from "@material-ui/icons/Add";
import { useDispatch } from "react-redux";
import { openSendMessage } from "../../redux/messageSlice";

export default function Sidebar({ messageCount }) {
  const dispatch = useDispatch();

  return (
    <div className="sidebar">
      <Button
        startIcon={<AddIcon fontSize="large" />}
        className="sidebar__compose"
        onClick={() => dispatch(openSendMessage())}
      >
        Compose
      </Button>
      <SidebarOption
        Icon={InboxIcon}
        title="Inbox"
        number={messageCount}
        selected={true}
      />
    </div>
  );
}
