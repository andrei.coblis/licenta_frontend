import { IconButton } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import DeleteIcon from "@material-ui/icons/Delete";
import axios from "axios";
import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import { selectOpenMessage } from "../../redux/messageSlice";
import Sidebar from "./Sidebar";
import "../../styles/message.css";

export default function Message() {
  const history = useHistory();
  const selectedMessage = useSelector(selectOpenMessage);

  async function handleDelete(messageId) {
    await axios.delete(`https://localhost:44392/api/v1/messages/${messageId}`);
    history.push("/Inbox");
  }

  return (
    <div className="message">
      <Sidebar />
      <div className="message__content">
        <div className="message__tools">
          <IconButton onClick={() => history.push("/Inbox")}>
            <ArrowBackIcon />
          </IconButton>
          <IconButton onClick={() => handleDelete(selectedMessage?.id)}>
            <DeleteIcon />
          </IconButton>
        </div>
        <div className="message__body">
          <div className="message__bodyHeader">
            <h2>{selectedMessage?.subject}</h2>
            <p>{selectedMessage?.title}</p>
            <p className="message__time">{selectedMessage?.time}</p>
          </div>
          <div className="message__text">{selectedMessage?.description}</div>
        </div>
      </div>
    </div>
  );
}
