import React, { useState } from "react";
import "../styles/styles.css";
import auth from "../services/authService";
import { useDispatch } from "react-redux";
import { login } from "../redux/userSlice";
export default function Login() {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const dispatch = useDispatch();

  function handleShowPassword(e) {
    e.preventDefault();
    var password = document.getElementById("passwordInput");
    password.type = password.type === "password" ? "text" : "password";
    e.target.value =
      e.target.value === "Show password" ? "Hide password" : "Show password";
  }

  async function handleSubmit(e) {
    e.preventDefault();
    const user = await auth.login(username, password);
    dispatch(
      login({
        username: username,
        password: password,
        role: user.Role,
      })
    );
    window.location = "/";
  }
  return (
    <form className="box" onSubmit={handleSubmit}>
      <h1>Login</h1>
      <input
        id="usernameInput"
        type="text"
        name=""
        placeholder="Username"
        onChange={(e) => setUsername(e.target.value)}
      ></input>
      <input
        id="passwordInput"
        type="password"
        name=""
        placeholder="Password"
        onChange={(e) => setPassword(e.target.value)}
      ></input>
      <input
        type="submit"
        value="Show password"
        onClick={handleShowPassword}
      ></input>
      <input type="submit" name="" value="Login"></input>
    </form>
  );
}
