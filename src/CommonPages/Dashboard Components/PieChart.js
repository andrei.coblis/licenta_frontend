import { Pie } from "react-chartjs-2";

export default function PieChart({ labels, data }) {
  const state = {
    labels: labels,
    datasets: [
      {
        label: "pie",
        backgroundColor: [
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 99, 132, 0.2)",
          "rgba(255, 206, 86, 0.2)",
          "rgba(75, 192, 192, 0.2)",
          "rgba(153, 102, 255, 0.2)",
          "rgba(255, 159, 64, 0.2)",
        ],
        borderColor: [
          "rgba(54, 162, 235, 1)",
          "rgba(255, 99, 132, 1)",
          "rgba(255, 206, 86, 1)",
          "rgba(75, 192, 192, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(255, 159, 64, 1)",
        ],
        borderWidth: 1,
        data: data,
      },
    ],
  };

  return (
    <div style={{ marginTop: "15px" }}>
      <Pie
        data={state}
        height={300}
        width={300}
        options={{ maintainAspectRatio: false }}
      />
    </div>
  );
}
