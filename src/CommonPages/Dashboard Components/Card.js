import "../../styles/card.css";
import ReactTooltip from "react-tooltip";

export default function Card({ cardTitle, cardValue, tooltipText, id }) {
  return (
    <div data-tip={tooltipText} data-for={id} className="card">
      <div className="card__inner">
        {/* <img src={icon} alt="Icon"></img> */}
        <p>{cardTitle}</p>
        <span>{cardValue}</span>
      </div>

      <ReactTooltip
        id={id}
        place="top"
        type="dark"
        effect="solid"
        delayShow="1000"
        multiline={true}
      ></ReactTooltip>
    </div>
  );
}
