import MessageList from "./Inbox Components/MessageList";
import Sidebar from "./Inbox Components/Sidebar";
import "../styles/inbox.css";
import { useState, useEffect } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import { selectUser } from "../redux/userSlice";
export default function Inbox() {
  const [messages, setMessages] = useState();
  const [messageCount, setMessageCount] = useState();
  const user = useSelector(selectUser);

  async function fetchData() {
    await axios
      .get(`https://localhost:44392/api/v1/UserMessages/${user.Username}`)
      .then((res) => {
        setMessages(res.data);
        setMessageCount(res.data.length);
      });
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="inbox">
      <Sidebar messageCount={messageCount} />
      <MessageList messages={messages} />
    </div>
  );
}
