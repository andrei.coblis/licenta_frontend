import { useEffect, useState } from "react";
import { Form, FormGroup, Table } from "react-bootstrap";
import axios from "axios";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { Button, IconButton } from "@material-ui/core";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import { Modal } from "react-bootstrap";
import ReactTooltip from "react-tooltip";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";
import "../styles/doctors.css";

export default function Doctors() {
  const [doctors, setDoctors] = useState();
  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);

  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [gender, setGender] = useState("Male");
  const [birthDate, setBirthDate] = useState();
  const [userId, setUserId] = useState();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleClose2 = () => setShow2(false);
  const handleShow2 = () => setShow2(true);

  async function fetchData() {
    await axios
      .get("https://localhost:44392/api/v1/doctors")
      .then((response) => {
        setDoctors(response.data);
      });
  }

  useEffect(() => {
    fetchData();
  }, []);

  async function handleEditDoctor(e) {
    e.preventDefault();
    await axios.put(`https://localhost:44392/api/v1/users/${userId}`, {
      username: username,
      password: password,
      role: "Doctor",
      firstName: firstName,
      lastName: lastName,
      gender: gender,
      birthDate: new Date(),
    }).then(() => alert('Success'))
    .catch((err) => alert(err));;
  }

  async function handleDelete(userId, doctorId) {
    const doctorsAfterDelete = doctors.filter((d) => d.id !== doctorId);
    await axios.delete(`https://localhost:44392/api/v1/users/${userId}`);
    setDoctors(doctorsAfterDelete);
  }

  async function handleCreateDoctor(e) {
    e.preventDefault();
    await axios.post("https://localhost:44392/api/v1/users", {
      username: username,
      password: password,
      role: "Doctor",
      firstName: firstName,
      lastName: lastName,
      gender: gender,
      birthDate: birthDate,
    }).then(() => alert('Success'))
    .catch((err) => alert(err));;
  }

  function actionFormatter(cell, row) {
    return (
      <div>
        <IconButton data-tip data-for="edit-tooltip">
          <EditIcon
            onClick={() => {
              setUsername(row.username);
              setPassword(row.password);
              setFirstName(row.firstName);
              setLastName(row.lastName);
              setGender(row.gender);
              setBirthDate(row.birthDate);
              setUserId(row.userId);
              handleShow2();
            }}
          />
        </IconButton>
        <ReactTooltip
          id="edit-tooltip"
          type="dark"
          effect="solid"
          delayShow="500"
        >
          Edit Doctor
        </ReactTooltip>

        <IconButton data-tip data-for="delete-tooltip">
          <DeleteIcon
            onClick={() => {
              handleDelete(row.userId, row.id);
            }}
          />
        </IconButton>
        <ReactTooltip
          id="delete-tooltip"
          type="dark"
          effect="solid"
          delayShow="500"
        >
          Delete Doctor
        </ReactTooltip>
      </div>
    );
  }

  const columns = [
    {
      dataField: "id",
      text: "Doctor Id",
      sort: true,
    },
    {
      dataField: "username",
      text: "Username",
    },
    {
      dataField: "firstName",
      text: "First Name",
    },
    {
      dataField: "lastName",
      text: "Last Name",
    },
    {
      dataField: "gender",
      text: "Gender",
      sort: true,
    },
    {
      dataField: "birthDate",
      text: "Birth Date",
      sort: true,
    },
    {
      dataField: "actions",
      text: "Actions",
      formatter: actionFormatter,
    },
  ];

  const { SearchBar, ClearSearchButton } = Search;

  function onBirthdateChange(date) {
    setBirthDate(date);
  }

  return (
    <div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
          }}
          closeButton
        >
          <div style={{ alignItems: "center" }}>
            <Modal.Title>Add Doctor</Modal.Title>
          </div>
        </Modal.Header>
        <Modal.Body>
          <Form className="text-center" style={{ width: "100%" }}>
            <FormGroup>
              <Form.Label>Enter Username</Form.Label>
              <Form.Control
                onChange={(e) => setUsername(e.target.value)}
              ></Form.Control>
              <Form.Label>Enter Password</Form.Label>
              <Form.Control
                onChange={(e) => setPassword(e.target.value)}
              ></Form.Control>
              <Form.Label>Enter First Name</Form.Label>
              <Form.Control
                onChange={(e) => setFirstName(e.target.value)}
              ></Form.Control>
              <Form.Label>Enter Last Name</Form.Label>
              <Form.Control
                onChange={(e) => setLastName(e.target.value)}
              ></Form.Control>
              <Form.Label>Select Gender</Form.Label>
              <Form.Control
                as="select"
                onChange={(e) => setGender(e.target.value)}
              >
                <option>Male</option>
                <option>Female</option>
              </Form.Control>
              <Form.Label>Enter Birth Date</Form.Label>
              <br />
              <DayPickerInput onDayChange={onBirthdateChange} />
              <br />
              <Button
                variant="primary"
                type="submit"
                onClick={handleCreateDoctor}
              >
                Create Doctor
              </Button>
            </FormGroup>
          </Form>
        </Modal.Body>
      </Modal>

      <div className="viewDoctors">
        {doctors && (
          <ToolkitProvider
            keyField="id"
            bootstrap4
            data={doctors}
            columns={columns}
            search
          >
            {(props) => (
              <div>
                <div className="table__header">
                  <div className="table__search">
                    <SearchBar {...props.searchProps} />
                    <ClearSearchButton {...props.searchProps} />
                  </div>
                  <Button
                    startIcon={<PersonAddIcon />}
                    style={{ float: "right" }}
                    onClick={handleShow}
                  >
                    New Doctor
                  </Button>
                </div>
                <hr />
                <BootstrapTable
                  {...props.baseProps}
                  pagination={paginationFactory()}
                  striped
                  hover
                />
              </div>
            )}
          </ToolkitProvider>
        )}
      </div>

      <Modal show={show2} onHide={handleClose2}>
        <Modal.Header
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
          }}
          closeButton
        >
          <div style={{ alignItems: "center" }}>
            <Modal.Title>Edit Patient</Modal.Title>
          </div>
        </Modal.Header>
        <Modal.Body>
          <Form className="text-center" style={{ width: "100%" }}>
            <FormGroup>
              <Form.Label>Enter Username</Form.Label>
              <Form.Control
                defaultValue={username}
                onChange={(e) => setUsername(e.target.value)}
              ></Form.Control>
              <Form.Label>Enter Password</Form.Label>
              <Form.Control
                onChange={(e) => setPassword(e.target.value)}
              ></Form.Control>
              <Form.Label>Enter First Name</Form.Label>
              <Form.Control
                defaultValue={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              ></Form.Control>
              <Form.Label>Enter Last Name</Form.Label>
              <Form.Control
                defaultValue={lastName}
                onChange={(e) => setLastName(e.target.value)}
              ></Form.Control>
              <Form.Label>Select Gender</Form.Label>
              <Form.Control
                as="select"
                onChange={(e) => setGender(e.target.value)}
                defaultValue={gender}
              >
                <option>Male</option>
                <option>Female</option>
              </Form.Control>
              <Form.Label>Enter Birth Date</Form.Label>
              <Form.Control
                defaultValue={birthDate}
                onChange={(e) => setBirthDate(e.target.value)}
              ></Form.Control>
              <Button
                variant="primary"
                type="submit"
                onClick={(e) => {
                  handleEditDoctor(e, userId);
                }}
              >
                Edit Doctor
              </Button>
            </FormGroup>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
