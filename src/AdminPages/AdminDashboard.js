import { useSelector } from "react-redux";
import { selectUser } from "../redux/userSlice";
import "../styles/adminDashboard.css";
import ProfilePicture from "../img/ProfilePicture.png";
import Card from "../CommonPages/Dashboard Components/Card";
import Select from "react-select";
import { useEffect, useState } from "react";
import { Button } from "reactstrap";
import ReactTooltip from "react-tooltip";
import { Modal } from "react-bootstrap";
import { Form, FormGroup } from "react-bootstrap";
import BarChart from "../CommonPages/Dashboard Components/BarChart";
import axios from "axios";

export default function AdminDashboard() {
  const user = useSelector(selectUser);
  const [barChart, setBarChart] = useState("Accuracy");
  const [models, setModels] = useState();
  const [modelName, setModelName] = useState();

  const [penalty, setPenalty] = useState();
  const [solver, setSolver] = useState();
  const [C, setC] = useState();

  const [nEstimators, setNEstimators] = useState();
  const [maxSamples, setMaxSamples] = useState();
  const [randomState, setRandomState] = useState();

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [selectedAlgorithm, setSelectedAlgorithm] = useState(
    "Logistic Regression"
  );
  const [usedAlgorithm, setUsedAlgorithm] = useState("Logistic Regression");
  const [usedAlgorithmOptions, setUsedAlgorithmOptions] = useState();

  const barChartOptions = [
    { value: "Accuracy", label: "Accuracy" },
    { value: "Precision", label: "Precision" },
    { value: "F1Score", label: "F1Score" },
    { value: "Recall", label: "Recall" },
    { value: "ROC_AUC_Score", label: "ROC_AUC_Score" },
  ];

  const algorithmOptions = [
    { value: "Logistic Regression", label: "Logistic Regression" },
    { value: "Bagging Ensemble", label: "Bagging Ensemble" },
  ];

  function renderBarChart() {
    var modelNames = [];
    var modelAccuracies = [];
    var modelPrecisions = [];
    var modelF1Scores = [];
    var modelRecallScores = [];
    var modelROCScores = [];
    for (var i = 0; i < models.length; i++) {
      modelNames[i] = models[i].name;
      modelAccuracies[i] = models[i].accuracy;
      modelPrecisions[i] = models[i].precision;
      modelF1Scores[i] = models[i].f1Score;
      modelRecallScores[i] = models[i].recall;
      modelROCScores[i] = models[i].roC_AUC_Score;
    }
    if (barChart === "Accuracy")
      return (
        <BarChart
          labels={modelNames}
          data={modelAccuracies}
          label="Accuracy Comparison"
        />
      );
    if (barChart === "Precision")
      return (
        <BarChart
          labels={modelNames}
          data={modelPrecisions}
          label="Precision Comparison"
        />
      );
    if (barChart === "F1Score")
      return (
        <BarChart
          labels={modelNames}
          data={modelF1Scores}
          label="F1Score Comparison"
        />
      );
    if (barChart === "Recall")
      return (
        <BarChart
          labels={modelNames}
          data={modelRecallScores}
          label="Recall Comparison"
        />
      );
    if (barChart === "ROC_AUC_Score")
      return (
        <BarChart
          labels={modelNames}
          data={modelROCScores}
          label="ROC_AUC Comparison"
        />
      );
  }

  async function handleTrainModel(e) {
    e.preventDefault();
    var parameters = [];
    if (selectedAlgorithm === "Logistic Regression") {
      parameters = [penalty, solver, C];
    }

    if (selectedAlgorithm === "Bagging Ensemble") {
      parameters = [nEstimators, maxSamples, randomState];
    }
    const payload = {
      type: selectedAlgorithm,
      parameters: parameters,
      name: modelName,
    };
    fetch("http://127.0.0.1:5000/trainAndSave", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(payload),
    }).then(() => alert("Success"))
    .catch((err) => alert(err));
  }

  async function fetchData() {
    await axios
      .get("https://localhost:44392/api/v1/mlAlgorithm")
      .then((res) => {
        setModels(res.data);
        var names = [];
        for (var i = 0; i < res.data.length; i++)
          names[i] = { value: res.data[i].name, label: res.data[i].name };
        setUsedAlgorithmOptions(names);
      });
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <main>
        <div className="main__container">
          {/* <!-- MAIN TITLE STARTS HERE --> */}

          <div className="main__title">
            <img src={ProfilePicture} alt="Profile" />
            <div style={{ width: "30%" }}>
              <div className="main__greeting">
                <h1>Hello, {user.Username}</h1>
                <p>Welcome to your dashboard</p>
              </div>
              {usedAlgorithmOptions && (
                <div className="main__info">
                  <Select
                    defaultValue={{
                      value: "Logistic Regression",
                      label: "Logistic Regression",
                    }}
                    options={usedAlgorithmOptions}
                    onChange={(e) => {
                      setUsedAlgorithm(e.value);
                      localStorage.setItem("algorithm", e.value);
                    }}
                  />
                  <Button
                    data-tip
                    data-for="new-model-tooltip"
                    onClick={() => {
                      handleShow();
                    }}
                  >
                    New model
                  </Button>
                  <ReactTooltip
                    id="new-model-tooltip"
                    type="dark"
                    effect="solid"
                    delayShow="500"
                  >
                    Create and train new algorithm model
                  </ReactTooltip>
                </div>
              )}
            </div>
          </div>

          {/* <!-- MAIN TITLE ENDS HERE --> */}

          {/* <!-- MAIN CARDS STARTS HERE --> */}
          <div className="main__cards_admin">
            <Card
              cardTitle="Model used"
              cardValue={usedAlgorithm}
              tooltipText="Model name"
              id={Math.random().toString()}
            />
          </div>

          {/* <!-- MAIN CARDS ENDS HERE --> */}

          {/* <!-- CHARTS STARTS HERE --> */}
          {models && (
            <div className="charts">
              <div className="charts__left">
                <div className="charts__left__title">
                  <div>
                    <h1>Model Performance Comparison</h1>
                    <p>Accuracy, Precision, F1Score, Recall, ROC_AUC_Score</p>
                    <Select
                      defaultValue={{ value: "Accuracy", label: "Accuracy" }}
                      options={barChartOptions}
                      onChange={(e) => {
                        setBarChart(e.value);
                      }}
                    />
                  </div>
                </div>
                {renderBarChart()}
              </div>
            </div>
          )}
        </div>
      </main>
      {selectedAlgorithm && (
        <Modal show={show} onHide={handleClose}>
          <Modal.Header
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              textAlign: "center",
            }}
            closeButton
          >
            <div style={{ alignItems: "center" }}>
              <Modal.Title>Configure new {selectedAlgorithm} model</Modal.Title>
            </div>
          </Modal.Header>
          <Modal.Body>
            <Form className="text-center" style={{ width: "100%" }}>
              <Form.Label>Select Model Type</Form.Label>
              <Select
                defaultValue={{
                  value: "Logistic Regression",
                  label: "Logistic Regression",
                }}
                options={algorithmOptions}
                onChange={(e) => {
                  setSelectedAlgorithm(e.value);
                }}
              />
              <Form.Label>Model Name</Form.Label>
              <Form.Control
                onChange={(e) => setModelName(e.target.value)}
              ></Form.Control>
              {selectedAlgorithm === "Logistic Regression" && (
                <FormGroup>
                  <Form.Label>Penalty</Form.Label>
                  <Form.Control
                    as="select"
                    onChange={(e) => setPenalty(e.target.value)}
                  >
                    <option>l1</option>
                    <option>l2</option>
                    <option>elasticnet</option>
                    <option>none</option>
                  </Form.Control>
                  <Form.Label>Solver</Form.Label>
                  <Form.Control
                    as="select"
                    onChange={(e) => setSolver(e.target.value)}
                  >
                    <option>newton-cg</option>
                    <option>lbfgs</option>
                    <option>liblinear</option>
                    <option>sag</option>
                    <option>saga</option>
                  </Form.Control>
                  <Form.Label>C Value</Form.Label>
                  <Form.Control
                    type="number"
                    onChange={(e) => setC(e.target.value)}
                  ></Form.Control>
                </FormGroup>
              )}
              {selectedAlgorithm === "Bagging Ensemble" && (
                <FormGroup>
                  <Form.Label>Number of Estimators</Form.Label>
                  <Form.Control
                    type="number"
                    onChange={(e) => setNEstimators(e.target.value)}
                  ></Form.Control>
                  <Form.Label>Maximum Samples</Form.Label>
                  <Form.Control
                    type="number"
                    onChange={(e) => setMaxSamples(e.target.value)}
                  ></Form.Control>
                  <Form.Label>Random State</Form.Label>
                  <Form.Control
                    type="number"
                    onChange={(e) => setRandomState(e.target.value)}
                  ></Form.Control>
                </FormGroup>
              )}
              <Button
                variant="primary"
                type="submit"
                onClick={handleTrainModel}
              >
                Train Model
              </Button>
            </Form>
          </Modal.Body>
        </Modal>
      )}
    </div>
  );
}
